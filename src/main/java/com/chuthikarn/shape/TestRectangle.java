/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chuthikarn.shape;

/**
 *
 * @author NB_MSI
 */
public class TestRectangle {
    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(3,4);
        System.out.println("Area of rectangle1 (w = " + rectangle1.getW() + " , l = " + rectangle1.getL() + ") is " + rectangle1.calArea());
        rectangle1.setWL(0, 4);
        System.out.println("Area of rectangle1 (w = " + rectangle1.getW() + " , l = " + rectangle1.getL() + ") is " + rectangle1.calArea());
        rectangle1.setWL(7, 9);
        System.out.println("Area of rectangle1 (w = " + rectangle1.getW() + " , l = " + rectangle1.getL() + ") is " + rectangle1.calArea());
    }
}
