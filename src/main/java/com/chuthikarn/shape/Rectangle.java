/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chuthikarn.shape;

/**
 *
 * @author NB_MSI
 */
public class Rectangle {
    private double w;
    private double l;
    public Rectangle(double w,double l) {
        this.w = w;
        this.l = l;
    }
    public double calArea() {
        return w * l;
    }
    public double getW() {
        return w;
    }
    public double getL() {
        return l;
    }
    public void setWL(double w,double l) {
        if (w <= 0) {
            System.out.println("Error: width must more than zero!!!!");
            return;
        }
        if (l <= 0) {
            System.out.println("Error: length must more than zero!!!!");
            return;
        }
        this.w = w;
        this.l = l;
    }
}
