/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chuthikarn.shape;

/**
 *
 * @author NB_MSI
 */
public class TestTriangle {
    public static void main(String[] args) {
        Triangle triangle1 = new Triangle(4,3);
        System.out.println("Area of Triangle (b = " + triangle1.getB() + " , h = " + triangle1.getH() + ") is " + triangle1.calArea());
        triangle1.setBH(8, 6);
        System.out.println("Area of Triangle (b = " + triangle1.getB() + " , h = " + triangle1.getH() + ") is " + triangle1.calArea());
        triangle1.setBH(0, 6);
        System.out.println("Area of Triangle (b = " + triangle1.getB() + " , h = " + triangle1.getH() + ") is " + triangle1.calArea());
    }
}
